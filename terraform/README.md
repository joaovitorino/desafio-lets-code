# Terraform para criar um cluster EKS


## Pre requisitos
- aws cli configurado com as devidas credenciais e região
- Terraform
- kubectl


## Configuração

Edite o arquivo [variables.tf](./variables.tf) para configurar a regiao onde sera criado o cluster.

Edite o arquivo [vpc.tf](./vpc.tf) para configurar a vpc se achar necessario

## Execução

Execute os comandos abaixo

`terraform init && terraform plan`

`terraform apply`

O comando abaixo irá utilizar o output do terraform para gerar um as informações necessarias para acessar e gerenciar o cluster usando kubectl

`aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)`

Verificar se o cluster esta funcionando

`kubectl cluster-info && kubctl get nodes`

Para destroir o cluster.

`terraform destroy`

